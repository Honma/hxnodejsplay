import js.node.http.ServerResponse;
class Router{

    var response : ServerResponse;

    public function doDefault()
    {
        response.end("HORRAY");
    }

    public function doFoo()
    {
        response.end("FOO=BAR");
    }

    public function new(response : ServerResponse){
        this.response = response;
    }    
}